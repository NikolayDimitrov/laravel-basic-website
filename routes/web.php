<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\MessagesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PagesController::class, 'getHome']);

Route::get('/about', [PagesController::class, 'getAbout']);

Route::get('/contact', [PagesController::class, 'getContact']);

Route::post('/contact/submit', 'App\Http\Controllers\MessagesController@submit');

Route::get('/messages', [MessagesController::class, 'getMessages']);