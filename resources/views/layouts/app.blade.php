<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Laravel Basic Website</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>
</head>
<body>
  @include('inc.navbar')

  <div class="container">
    @if(Request::is('/'))
      @include('inc.showcase')
    @endif    
    <div class="row">
      <div class="col-md-8 col-lg-8">
        @include('inc.messages')
        @yield('content')
      </div>
      <div class="col-md-4 col-lg-4">
      @include('inc.sidebar')
      </div>
    </div>
  </div>  

  <footer id="footer" class="text-center text-light bg-dark" style="padding:20px; margin-top:30px;">
    <p>Copyright 2023 &copy;Laravel Basic Website</p> 
  </footer>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</body>
</html>