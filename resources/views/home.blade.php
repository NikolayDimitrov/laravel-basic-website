@extends('layouts.app')

@section('content')
<h1 class="mt-4">Home</h1>
<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quibusdam excepturi rerum aspernatur officiis quos quaerat provident, ratione vel repellat consectetur sequi quo, soluta animi nam labore accusamus quisquam. Reiciendis, nemo.</p>
@endsection

@section('sidebar')
  @parent
  <p>This is appended to the sidebar</p>
@endsection